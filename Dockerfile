FROM debian:stretch

ADD ./pennidinhhomehub-apacheserver-mod_auth_openidc_lwa_fork /source/

RUN cd /source && apt-get update && apt-get --yes install autoconf curl pkg-config libcurl4-openssl-dev libssl-ocaml-dev libjansson-dev libcjose-dev libpcre3-dev make apache2-dev apache2 redis-tools

RUN apt-get --yes install ruby2.3 ruby2.3-dev

RUN gem install sys-filesystem 
RUN gem install redis -v 4.4.0

RUN cd /source && autoconf
RUN cd /source && autoreconf
RUN cd /source && ./configure --with-apxs2=/usr/bin/apxs2
RUN cd /source && make
RUN cd /source && make install

RUN rm -rf /etc/apache2/
COPY ./pennidinhhomehub-apacheserver-config /etc/apache2/

RUN apt-get update
RUN apt-get install --yes php

COPY ./run.sh /
RUN chmod 777 /run.sh

RUN rm -rf /var/www/html/
COPY ./pennidinhhomehub-apacheserver-documentroot/_h5ai /var/www/html/_h5ai
COPY ./pennidinhhomehub-apacheserver-documentroot/secure/assets /var/www/html/secure/assets
COPY ./pennidinhhomehub-apacheserver-documentroot/*.php /var/www/html/
COPY ./pennidinhhomehub-apacheserver-documentroot/*.ico /var/www/html/
COPY ./pennidinhhomehub-apacheserver-documentroot/*.png /var/www/html/
COPY ./pennidinhhomehub-apacheserver-documentroot/*.html /var/www/html/
COPY ./pennidinhhomehub-apacheserver-documentroot/unsecure /var/www/html/unsecure
COPY ./pennidinhhomehub-apacheserver-documentroot/secure /var/www/html/secure

CMD /run.sh
