#!/usr/bin/env sh

while [ ! -f $SSL_CERTIFICATE_FILE ]
do
  echo "Sleeping 2 seconds waiting for $SSL_CERTIFICATE_FILE"
  sleep 2
done

while [ ! -f $SSL_CERTIFICATE_KEY_FILE ]
do
  echo "Sleeping 2 seconds waiting for $SSL_CERTIFICATE_KEY_FILE"
  sleep 2
done

echo "SSL Certs exist. Generating apache conf files..."

cd /etc/apache2/sites-enabled/ && /usr/bin/erb 001-default-external.conf.erb > 001-default-external.conf
if [  $? != 0 ]; then 
  echo "Generating .conf files with ERB failed. Exiting..."
  exit
fi

echo "Apache conf files generated. Fetching internal and external domain name..."

ln -s $OPENVPN_HTTP_DIR /var/www/html/secure/openvpn

internalDomain=`redis-cli -h redis get internal-subdomain`
if [  $? != 0 ]; then 
  echo "Fetching internal domain from redis failed. Exiting..."
  exit
fi
externalDomain=`redis-cli -h redis get external-subdomain`
if [  $? != 0 ]; then 
  echo "Fetching external domain from redis failed. Exiting..."
  exit
fi
while [ [-z "$internalDomain"] || [-z "$externalDomain"] ]
do
  echo "Sleeping 2 seconds waiting for internal and external subdomains"
  sleep 2
  internalDomain=`redis-cli -h redis get internal-subdomain`
  if [  $? != 0 ]; then 
    echo "Fetching internal domain from redis failed. Exiting..."
    exit
  fi
  externalDomain=`redis-cli -h redis get external-subdomain`
  if [  $? != 0 ]; then 
    echo "Fetching external domain from redis failed. Exiting..."
    exit
  fi
done

echo "export SSL_CERTIFICATE_FILE=$SSL_CERTIFICATE_FILE" >> /etc/apache2/envvars
echo "export SSL_CERTIFICATE_KEY_FILE=$SSL_CERTIFICATE_KEY_FILE" >> /etc/apache2/envvars
echo "export EXTERNAL_DOMAIN_NAME=$externalDomain" >> /etc/apache2/envvars
echo "export INTERNAL_DOMAIN_NAME=$internalDomain" >> /etc/apache2/envvars

echo "starting apache..."

/usr/sbin/apachectl start

lastSave=`redis-cli -h redis get restart-apache`
lastSaveHard=`redis-cli -h redis get hard-restart-apache`

while true
do
  latestLastSave=`redis-cli -h redis get restart-apache`
  latestLastSaveHard=`redis-cli -h redis get hard-restart-apache`

  if [ "$lastSave" != "$latestLastSave" ]
  then
    echo "Redis has been updated since last check"
    lastSave=$latestLastSave
    cd /etc/apache2/sites-enabled/ && /usr/bin/erb 001-default-external.conf.erb > 001-default-external.conf
    /usr/sbin/apachectl graceful
  fi

  if [ "$lastSaveHard" != "$latestLastSaveHard" ]
  then
    echo "Redis has been updated since last check"
    lastSaveHard=$latestLastSaveHard
    # graceful doesn't reload SSL certs
    exit
  fi

  sleep 10
done
